Lead: @1337bytes (unless any other takers?)
Contact methods: [Twitter](https://twitter.com/1337bytes), [GitHub](https://github.com/macintoshhelper)
Description of group: Prototype an open and unopinionated design specification that allows one to build the front-end of an app or design system on top of a protocol like GUN/Mastodon.

Next goals - either link to [Taiga](http://taiga.whatscookin.us/project/bluesky-community/kanban) task/epics or include in the markdown, a list of (TODO)

## Deliverable:

Create a React app with a reusable/protocol agnostic UI component library, using GUN for user authentication and submitting a post to your profile

## ETA:

TODO

## Assignee(s):

**Add Your Name Below:**
- 
- 

